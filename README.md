# bird-screen-url

填入想隨機分配的網址，程式會把隨機挑選並把使用者重新導向至雀屏中選的網址

![雀屏中選.png](./雀屏中選_英文-Google搜尋.png)

## 使用方式
替換 [`public/url.js`](public/url.js) 裡面的網址成想隨機分配的網址並提交至預設分支，其他不需變更。

之後會自動觸發 GitLab CI 來發布 pages。

### 教學文
不才寫的一篇簡單教學文──[〈隨機分配問卷？讓 GitLab Pages 幫你吧！〉](https://hhming.moe/post/random-survey-gitlab-pages-ver/)，懇請各方斧正。

### 範例
![](https://img.shields.io/gitlab/pipeline-status/hms5232/bird-screen-url?branch=main)

本儲存庫的 GitLab Pages 將會依照 `public/url.js` 隨機將使用者導向本人的 GitLab、Github、我的個人網站等地方。👇  
https://hms5232.gitlab.io/bird-screen-url

## 其他版本

### Github
如果比較常用 Github 的話，可以參考 Github pages 版本：https://github.com/hms5232/random-survey

## LICENSE
[MIT](LICENSE)
